# Opening a file with the cursor at the end ready for text

If you want a script to make a template and then open vim on that template ready for the user to start writing:

```
vim "+normal G" +startinsert "$filename"
```

You can't send commands to vim directly through stdin as it gets upset about not being attached to an interactive terminal. It's possible that vi is OK with this though.
