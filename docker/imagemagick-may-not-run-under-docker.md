# ImageMagick may not run under docker

I have a Perl program that uses the ImageMagick library. However running it under docker returns:

    libgomp: Thread creation failed: Operation not permitted

I'm not sure exactly why this happens, but running docker with `--priviliged` solves it. But opens up other risks, so use with caution.

Slightly later edit:

From <https://github.com/bytebase/bytebase/issues/3096>, `--security-opt seccomp=unconfined` also seems to work and some testing confirms this.
