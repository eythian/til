# Setting the wifi country on raspberry pi boot

## Problem

My wifi router has the 2.4GHz channel set to "auto" so it can find a fairly
quiet channel on its own. One day it decided to use channel 13, which is fine,
I didn't notice the change. Then I noticed my raspberry pi (headless, running
Debian bookworm) was no longer online. 

## Diagnosis and Cause

The default regulatory domain on boot is "unset", which gives you channels
1-11. If I manually set it (`iw reg set NL`), then I can see 12 and 13 also,
but this isn't the case at boot. I also found that if I let it get online (by
forcing my router to a lower channel) it would pick up the regulatory domain,
and so could handle the higher channels. But if the wifi router was on one
outside of "unset" and the raspberry pi rebooted, it was blind. In previous
versions there was a package called `crda` that did this for you, but it's gone
and nothing seems to have done the setting in its place. Which is weird.

## Solution

This is a bit hacky but it works and I couldn't find a better way. In the file
`/boot/firmware/cmdline.txt`, append `cfg80211.ieee80211_regdom=XX`, but change
`XX` to be your country code in capitals. DE, GB, NZ, or whatever.

## Reference

* https://bugs.launchpad.net/netplan/+bug/1951586
