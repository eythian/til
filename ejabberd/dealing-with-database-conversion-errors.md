# Dealing with database conversion errors

When migrating ejabberd from one host to another, you can get issues with differing hostnames. The documentation to migrate the database from one host to another is here: https://docs.ejabberd.im/admin/guide/managing/#change-computer-hostname (down the bottom of this page.)

I found that when doing this on Debian Buster, I got errors:

```
Error: {"Backup traversal failed",enoent}
```

and

```
Error: {"Backup traversal failed",eacces}
```

both of these largely useless errors were caused by where I had put the backup file to be read, and the converted one to be written to.

The example in the documentation uses `/tmp`, however my guess is that this is sandboxed from `ejabberd` on Debian. Instead what worked for me was to put the file in to `/var/lib/ejabberd` and not use absolute pathnames to refer to them. So the command became:

```
ejabberdctl mnesia_change_nodename ejabberd@old-hostname ejabberd@new-hostname ejabberd.backup ejabberd.new.backup
```
