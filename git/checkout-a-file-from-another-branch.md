# Checkout a file from another branch

Another overloading of `git checkout`:

```
git checkout <branch name> -- <files ...>
```
