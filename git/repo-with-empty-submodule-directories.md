# Repo with empty submodule directories

If you find that your repo has empty directories where there should be checked out submodules, this can be fixed with:

```
git submodule update --init
```
