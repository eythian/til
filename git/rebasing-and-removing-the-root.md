# Rebasing and removing the root

If you need to edit history, and the commit that you want to remove is the very first commit in a repo, you need to use the `--root` option to rebase:

```
$ git rebase -i --root
```

## References

* https://stackoverflow.com/questions/45249813/how-to-delete-the-first-commit-and-make-its-immediate-child-the-root
