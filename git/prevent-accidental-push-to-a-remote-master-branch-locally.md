# Prevent accidental push to a remote master branch locally

I usually don't want to push to master, even if the upstream allows it. However if I'm doing a quick fix for something I often forget to make a branch first and push there by accident.

To stop this, I put a local `pre-push` hook in place that will force me to confirm that this is really what I want to do. This file goes in `.git/hooks/pre-push`. Don't forget to `chmod +x` it, or copy the below into your terminal.

```
cat << 'EOH' > .git/hooks/pre-push
#!/bin/bash

protected_branch='master'
current_branch=$(git symbolic-ref HEAD | sed -e 's,.*/\(.*\),\1,')

if [ $protected_branch = $current_branch ]
then
    read -p "You're about to push master, is that what you intended? [y|n] " -n 1 -r < /dev/tty
    echo
    if echo $REPLY | grep -E '^[Yy]$' > /dev/null
    then
        exit 0 # push will execute
    fi
    exit 1 # push will not execute
else
    exit 0 # push will execute
fi
EOH
chmod +x .git/hooks/pre-push
```

## References

* https://ghost.org/changelog/prevent-master-push/
