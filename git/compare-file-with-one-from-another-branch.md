# Compare file with one from another branch

Sometimes when working on a feature branch with a number of commits, I want to see what's changed compared to `master` in a particular file.

```
git diff master -- path/to/file
```
