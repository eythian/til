# Creating default git hooks

When a new repo is cloned or init'ed, git can put your own files into the `.git` directory that it created. This means that you can also have it provide some default hooks.

The git config option is `init.templatedir`, everything that's in the provided directory will be copied into the newly created `.git` directory. To use this for hooks, simply create a `hooks` subdirectory in here.

```
mkdir -p ~/.git-templates/hooks
git config --global init.templatedir '~/.git-templates'
cp pre-hook ~/.git-templates/hooks
chmod +x ~/.git-templates/hooks/pre-hook
```

If you run `git init` on an existing git repo, it will copy the new templates into that.

## References

* https://coderwall.com/p/jp7d5q/create-a-global-git-commit-hook
