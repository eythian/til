# Force alt-tab to apply only to the current workspace

Gnome by default will have alt-tab apply across all workspaces. This sometimes means it'll change workspaces on you if a window was last used there, which I find annoying. You can make alt-tab only apply to the current workspace by doing:

```
gsettings set org.gnome.shell.app-switcher current-workspace-only true
```
