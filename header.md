# Robin's Today-I-Learnts

Little notes about things that I want to write down because the next time I'm likely to need them, I'll have forgotten the details.

Want to use this? Just clone it, delete the things you don't want, and symlink `til` to somewhere that's in your path. Then run `til category` to get started. Tested with vim on Linux.

