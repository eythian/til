# Heredocs in bash

I always forget the syntax of these, even though it's pretty simple. Especially when redirecting output.

```
$command <<TEMPLATE > myfile.txt
This is the content.
TEMPLATE
```

Beware that if you just want to send that output straight to the file, use `cat`, not `echo`.

## Interpolation of variables

By default, heredocs will interpolate variables in them. This can be avoided by wrapping the end marker in single-quotes.

```
$command <<'TEMPLATE' > myfile.txt
This is the $literal content.
TEMPLATE
```
