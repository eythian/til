# `getopt` and `getopts` are different things

`getopt` and `getopts` are both things for parsing command-line options in shell scripts.

`getopts` is a Bash built-in and doesn't support a lot of things, like long arguments.

`getopt` comes with `util-linux` and is an external tool to parse arguments, and has a lot of additional features.

In general, `getopt` is the better choice for anything that's not super-simple. Sadly the man page (on Ubuntu at least) contains no examples, but there [are some here](https://github.com/karelzak/util-linux/blob/master/misc-utils/getopt-parse.bash).
