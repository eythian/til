# Have a script execute in its own directory

If a script needs to access its own files, but it can be run from anywhere due to paths and/or symlinks, you can have it `cd` to its own directory and run from there.

```
myname=$( readlink -f $0 )
mydirname=$( dirname $myname )
cd $mydirname
```

The `readlink` dereferences any symlinks from `$0` (the name and path of the script), and the `dirname` gets the directory component.

The `cd` is done in a sub-process, so will be undone when it exits, so there's no need to save it.

`readlink -f` might not work on non-GNU things like OSX.

