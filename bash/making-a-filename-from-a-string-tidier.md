# Making a filename from a string tidier

If you want "This is 'a' test" to be "this-is-a-test", including dealing with repeats and other stuff:

```
filename=$( echo "$title" | sed -e 's/ /-/g' | tr '[:upper:]' '[:lower:]' | tr -cd '[:alnum:]-' )
```
