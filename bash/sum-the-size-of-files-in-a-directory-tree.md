# Sum the size of files in a directory tree

Using `du` you can get the on-disk space used by files, which includes their block allocation. This is most useful when determining required storage space, however if you're interested in how many bytes you've actually written or similar, it's perhaps not what you want. It will also include the block size of directory entries.

Instead, by using `find` you can produce a list of files, pass that list to `du` and have it work with them.

```
find . -type f -print0 | du -scb --files0-from=-
```

You can also easily exclude paths using `find`'s arguments, perhaps you don't want to include the git repo:

```
find . -type f "!" -path "*.git*"  -print0 | du -scb --files0-from=-
```

