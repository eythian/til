# Blocking spammy TLDs

`.xyz` and `.icu` have been the source of most of my spam, and a lot of it was getting through my spam filter. So now they're getting a really high spammyness score which causes them to get rejected immediately.

In `/etc/spamassassin/local.cf:

```
# Reject spammy domains
header SPAMMY_TLD_IN_RCVD Received =~ /\.(icu|xyz)\s/i
score SPAMMY_TLD_IN_RCVD 4.0
describe SPAMMY_TLD_IN_RCVD Spammy TLD used in Received line

header SPAMMY_TLD_IN_FROM From =~ /\.(icu|xyz)>$/i
score SPAMMY_TLD_IN_FROM 4.0
describe SPAMMY_TLD_IN_FROM Spammy TLD used in From line

uri SPAMMY_TLD_URI /\.(icu|xyz)\/.+/i
score SPAMMY_TLD_URI 4.0
describe SPAMMY_TLD_URL Spammy TLD used in link
```

This gives a score of 4 to one of those TLDs being seen in each of the `Received` and `From` headers, and in a URL in the body. For my filtering (where 5+ goes into the spam folder, 8+ gets rejected by postfix) it'd have to have a lot of good attributes to get accepted at all. These numbers could probably be reduced to 1.0 or 2.0 and it'd still work well, but I don't know of any legitimate emails with these domains, so I'm OK with the false positive risk.
