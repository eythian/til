# Clear to the end of the line when over-printing

If you want to have an updating display that overwrites itself, say printing out a line of files as they're processed, but sometimes the names get shorter and that makes things ugly, then you can use the escape sequence `\e[K` to clear the rest of the line.

```
print "Longlonglonglonglong\e[K\r";
print "Short."
```

