# Catching control-c in a Perl SDL application

I have [an application](https://gitlab.com/eythian/tracker) that uses Perl's SDL bindings to periodically render output of a long-running process. So, most of the time it's not running the normal SDL event loop.

By default, SDL catches ctrl-c (`SIGINT`) and turns it into an event. This meant it could be some time before the program shut down, which was bad.

To keep the normal behaviour of control-c handling (i.e. terminate immediately), you can localise the handler so that it'll be restored to what it was before SDL changed it.

```
sub init {
    local $SIG{INT};

    my $app = SDLx::App->new(
        ...
    );
}
```

SDL has some flags such as `SDL_INIT_NOPARACHUTE` and `SDL_HINT_NO_SIGNAL_HANDLERS`, but I couldn't make them work for me.
