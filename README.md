# Robin's Today-I-Learnts

Little notes about things that I want to write down because the next time I'm likely to need them, I'll have forgotten the details.

Want to use this? Just clone it, delete the things you don't want, and symlink `til` to somewhere that's in your path. Then run `til category` to get started. Tested with vim on Linux.

## ansible

* [Matching operating system and distribution versions](./ansible/matching-operating-system-and-distribution-versions.md)

## bash

* [`getopt` and `getopts` are different things](./bash/getopt-and-getopts-are-different-things.md)
* [Have a script execute in its own directory](./bash/have-a-script-execute-in-its-own-directory.md)
* [Heredocs in bash](./bash/heredocs-in-bash.md)
* [Making a filename from a string tidier](./bash/making-a-filename-from-a-string-tidier.md)
* [Sum the size of files in a directory tree](./bash/sum-the-size-of-files-in-a-directory-tree.md)

## docker

* [ImageMagick may not run under docker](./docker/imagemagick-may-not-run-under-docker.md)

## ejabberd

* [Dealing with database conversion errors](./ejabberd/dealing-with-database-conversion-errors.md)

## fortune

* [Maintaining a fortune quote file](./fortune/maintaining-a-fortune-quote-file.md)

## git

* [Checkout a file from another branch](./git/checkout-a-file-from-another-branch.md)
* [Compare file with one from another branch](./git/compare-file-with-one-from-another-branch.md)
* [Creating default git hooks](./git/creating-default-git-hooks.md)
* [Prevent accidental push to a remote master branch locally](./git/prevent-accidental-push-to-a-remote-master-branch-locally.md)
* [Rebasing and removing the root](./git/rebasing-and-removing-the-root.md)
* [Repo with empty submodule directories](./git/repo-with-empty-submodule-directories.md)

## gnome

* [Force alt-tab to apply only to the current workspace](./gnome/force-alt-tab-to-apply-only-to-the-current-workspace.md)

## perl

* [Catching control-c in a Perl SDL application](./perl/catching-control-c-in-a-perl-sdl-application.md)
* [Clear to the end of the line when over-printing](./perl/clear-to-the-end-of-the-line-when-over-printing.md)

## postfix

* [Opportunistic TLS on outgoing email](./postfix/opportunistic-tls-on-outgoing-email.md)

## spamassassin

* [Blocking spammy TLDs](./spamassassin/blocking-spammy-tlds.md)

## til-itself

* [Having a public/private shared til repo pair](./til-itself/having-a-publicprivate-shared-til-repo-pair.md)

## time

* [Convert epoch time to local time](./time/convert-epoch-time-to-local-time.md)

## vim

* [Opening a file with the cursor at the end ready for text](./vim/opening-a-file-with-the-cursor-at-the-end-ready-for-text.md)

## wifi

* [Setting the wifi country on raspberry pi boot](./wifi/setting-the-wifi-country-on-raspberry-pi-boot.md)

