# Opportunistic TLS on outgoing email

A while ago I had problems with gmail marking all my emails as spam. I had set all the SPF on DNS, made sure that all my IP addresses had correct reverse entries (including the IPv6!), but still things were getting put in the spam bucket.

Eventually I saw someone mention in passing that using TLS when sending email worked for them, so I configured it and it worked for me too.

Enabling this on postfix is easy, just add the following to your `/etc/postfix/main.cf`:

```
smtp_tls_security_level=may
```
