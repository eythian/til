# Maintaining a fortune quote file

Fortune files are simple text files, with each quote being separated by a `%` on its own line.

    "Gentlemen, you can't fight in here. This is the war room."
      -- President Merkin Muffley (Peter Sellers), Dr. Strangelove
    %
    git gets easier once you get the basic idea that branches are homeomorphic endofunctors mapping submanifolds of a Hilbert space.
    %
    Coach: "How's a beer sound, Norm?"
    Norm: "I don't know, I usually finish before they get a word in."
      -- Coach (Nicholas Colasanto) and Norm (George Wendt), Cheers

However, fortune itself reads from a hash file, this can be generated:

    strfile quotes quotes.dat

Now pass the filename (or a directory containing multiple files) when running `fortune`.
