# Convert epoch time to local time

On Linux:

    date -d @1606400206

On BSD:

    date -r 1606400206

## References

* https://unix.stackexchange.com/a/2993
