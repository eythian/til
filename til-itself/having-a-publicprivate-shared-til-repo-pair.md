# Having a public/private shared til repo pair

I want to have two separate repos containing TILs.

One is public and contains general stuff, the other is private and contains stuff specific to work systems that has little point being public.

It is possible to do this with Git, having one branch that upstreams to the public repo, and another that upstreams to the private one. Then using some wrapper scripts, the relevant branch can be selected.

In my configuration, on my work computer, I kept `master` being the private repo, pointing to the master branch there. I then created a new branch called `public`, and set its upstream to the public repo's `master`. In the end I did this by editing `.git/config` directly, but it might be possible to do it with the command line options.

My full `.git/config` options:

```
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
[push]
    default = upstream
[rerere]
    enabled = true
[remote "origin"]
	url = git@work.gitlab.server/til.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[branch "master"]
	remote = origin
	merge = refs/heads/master
[branch "public"]
    remote = public
    merge = refs/heads/master
[remote "public"]
	url = git@gitlab.com:eythian/til.git
	fetch = +refs/heads/*:refs/remotes/public/*
```

`push.default` is to stop Git complaining about the local (`public`) and upstream (`master`) branch names not matching. `rerere.enabled` helped deal with repeated merge conflicts when I was testing, it's probably not so useful in normal usage.

## Wrapper scripts

I made two wrapper scripts: `tilw` for work (private) stuff, and `tilp` for public stuff. These scripts are:

`tilp`:

```
#!/bin/bash

set -e

# This adds a til to the public branch (called public locally), and also ensures
# that everything is up to date
cd ~/git_tree/til

git fetch --multiple origin public
git checkout public
git pull

./til "$@"

# Now merge this in to the work branch (called master)
git checkout master
git merge --no-edit public
git push
```

`tilw`:

```
#!/bin/bash

set -e

# This adds a til to the work branch (called master locally), and also ensures
# that everything is up to date
cd ~/git_tree/til

git fetch --multiple origin public
git checkout master
git pull
git merge --no-edit public/master

./til "$@"
```

These scripts ensure that everything is up to date, and will merge the public repo into the private one. Currently the base `til` script doesn't check for updates, it should probably be made to do so.

## Caveats

When adding tils to the work repo, I often got merge conflicts on the `README.md` file, however I think this may not occur so much in normal use.
