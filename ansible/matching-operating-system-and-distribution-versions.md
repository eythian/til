# Matching operating system and distribution versions

It's not uncommon to want to handle things differently in ansible for a range of distribution versions. This can be done using ansible's conditionals, in particular `when`.

If you want to do greater-than or less-than type comparisons, you need to convert the provided string version number to an int with `|int`.

```
- name: version specific thing
  ...
  when: ansible_os_family == "Debian" and ansible_lsb.major_release|int >= 10
```
